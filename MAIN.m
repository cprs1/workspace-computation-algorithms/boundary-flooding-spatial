%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
% FILE_6RFSrobotfile
% FILE_6EFSrobotfile
% FILE_6EFRrobotfile
% FILE_3RFSrobotfile
FILE_6EFRdisk_robotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 0; % set 1 to see how it works during computation
[WK,outstruct] = FCN_BoundaryFloodingSpatialWK(fcn,params,instantplot);

%%

volumetot = FCN_plot_space(WK,params);

%% slice

slice_coord = 0.55;
str = 'XY';

PlotSlice(WK,str,slice_coord);

% % slices video
% slice_coord = -1;
% str = 'XY';
% figure()
% while slice_coord<1
% 
% slice_coord = slice_coord + .01;
% [area,h] = PlotSlice(WK,params,str,slice_coord);
% title([str, 'Slice', num2str(slice_coord),'m'])
% pause(.1)
% delete(h)
% drawnow
% end

% save WK WK;
% save volumetot volumetot;
% save outstruct outstruct;