%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get uniform grid in the plane

% INPUT:
% params: structure with simulation parameters

% OUTPUT:
% grid: array for points in the grid

function grid = defineGrid(params)
    boxsize = params.boxsize;
    stepsize_x = params.stepsize_x;
    stepsize_y = params.stepsize_y;
    stepsize_z = params.stepsize_z;
    xU = boxsize(1);
    xB = boxsize(2);
    yU = boxsize(3);
    yB = boxsize(4);
    zU = boxsize(5);
    zB = boxsize(6);

    % x
    n_sampX = floor(abs(xU-xB)/stepsize_x);
    x = linspace(xU,xB,n_sampX);
    if n_sampX==0
        n_sampX = 1;
        x = (xU+xB)/2;
    end

    % y
    n_sampY = floor(abs(yU-yB)/stepsize_y);
    yy = linspace(yU,yB,n_sampY);
    if n_sampY==0
        n_sampY = 1;
        yy = (yU+yB)/2;
    end

    % z
    n_sampZ = floor(abs(zU-zB)/stepsize_z);
    z = linspace(zU,zB,n_sampZ);
    if n_sampZ==0
        n_sampZ = 1;
        z = (zU+zB)/2;
    end

    [X,Y,Z] = meshgrid(x,yy,z);
    Xp = reshape(X,n_sampX*n_sampY*n_sampZ,1);
    Yp = reshape(Y,n_sampX*n_sampY*n_sampZ,1);
    Zp = reshape(Z,n_sampX*n_sampY*n_sampZ,1);
    grid = [Xp,Yp,Zp];
end