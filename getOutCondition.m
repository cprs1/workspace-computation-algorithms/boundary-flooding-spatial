%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to individuate boundary condition after a IGSP computation
% INPUT:
% y: robot configuration
% jac: jacobian matrix of the IGSP problem
% pend: end effector position
% solve_flag: solver flag (fsolve, if converged or not)
% norminv: infinite norm of inv(J), to avoid T1-jumping
% params: structure with simulation parameters
% fcn: structure with objective functions

% OUTPUT:
% exitflag: boolean flag: 1 = in workspace, 0 = out-of-workspace
% exitifx: idx that indicates which exitcondition occured

function [exitflag,exitidx] = getOutCondition(y,jac,solve_flag,norminv,params,fcn)

TOL2 = params.TOL2;
TOL  = params.TOL;
Stability = fcn.stabilityfcn;
Singu = fcn.singufcn;
mech = fcn.mechconstrfcn;
% compute border-conditions
sol_flag = ((solve_flag == 1) || (solve_flag == 2) || (solve_flag == 3) || (solve_flag == 4));
flag_norm = norminv<=TOL2;
[T1,T2] = Singu(jac,y);
t1_flag = T1>TOL;
t2_flag = T2>TOL;
stab = Stability(jac,y);
mechflag = mech(y);

                
% understand exit condition
if sol_flag~=1
    % 2. solver fails
    exitidx = 2;
    exitflag = 0;
elseif flag_norm~=1
    % 3. norm inv too high
    exitidx = 3;
    exitflag = 0;
elseif t1_flag~=1
    % 4.type 1 singu
    exitidx = 4;
    exitflag = 0;
elseif t2_flag~=1
    % 5.type 2 singu
    exitidx = 5;
    exitflag = 0;
elseif stab~=0
    % 6. stab change
    exitidx = 6;
    exitflag = 0;
elseif mechflag~=1
    % 7. mech limits
    exitidx = 7;
    exitflag = 0;
else
    % 1. point in the wk
    exitidx = 1;
    exitflag = 1;    
end
                
end