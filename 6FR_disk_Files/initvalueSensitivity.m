function yd = initvalueSensitivity(s,y,L,qei,Nf)
h = y(1+3:3+4,1);
dhdp0 = reshape(y(1+3+4+3*3:3+4+3*3+3*4,1),4,3);
dhdh0 = reshape(y(1+3+4+3*3+4*3+3*4:3+4+3*3+4*3+3*4+4*4,1),4,4);

R = quat2rotmatrix(h);
Phi = PhiMatr(s,1,Nf);
k = Phi*qei;
k1 = k(1); k2 = k(2); k3 = k(3);
A = [0,-k1,-k2,-k3;
    +k1,0,+k3,-k2;
    +k2,-k3,0,+k1;
    +k3,+k2,-k1,0];
Dh = 2*[+h(3),+h(4),+h(1),+h(2);-h(2),-h(1),+h(4),+h(3);+h(1),-h(2),-h(3),+h(4)];

% derivatives
pd = R(:,3);
hd = 0.5*A*h;
dpdp0d = Dh*dhdp0;
dhdp0d = 0.5*A*dhdp0;
dpdh0d = Dh*dhdh0;
dhdh0d = 0.5*A*dhdh0;

yd = L*[pd;hd;reshape(dpdp0d,3*3,1);reshape(dhdp0d,4*3,1);reshape(dpdh0d,3*4,1);reshape(dhdh0d,4*4,1);];
end