function flag = StabilityMode6EFR_1c(jac,Nfm,y,rotparams)
% Nf = 3*Nfm;
% 
% qp = y(1+6+2*6*Nf + Nf+6+6:6+2*6*Nf + Nf+6+6+6);
% qd = y(1+6+2*6*Nf + Nf+6  :6+2*6*Nf + Nf+6+6,1);
% Dplat = rot2twist(qp(4:6,1),rotparams);
% Ddisk = rot2twist(qd(4:6,1),rotparams);
% jac(1+2*6*Nf+Nf+6+3  :2*6*Nf+Nf+6+6,:)   = Ddisk'*jac(1+2*6*Nf+Nf+6+3   :2*6*Nf+Nf+6+6,:); % disk equilibrium
% jac(1+2*6*Nf+Nf+6+6+3:2*6*Nf+Nf+6+6+6,:) = Dplat'*jac(1+2*6*Nf+Nf+6+6+3 :2*6*Nf+Nf+6+6+6,:); % platform equilibrium
% 
% % Ubeams = jac(1:2*6*Nf+Nf,1+6:6+2*6*Nf+Nf);
% % Uwrenc = jac(1+2*6*Nf+Nf+6:2*6*Nf+Nf+6+6+6,1+6:6+2*6*Nf+Nf);
% % U = [Ubeams;Uwrenc];
% 
% % Pbeams = jac(1:2*6*Nf+Nf,1+6+2*6*Nf+Nf+6:6+2*6*Nf+Nf+6+6+6);
% % Pwrenc = jac(1+2*6*Nf+Nf+6:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6:6+2*6*Nf+Nf+6+6+6);
% % P = [Pbeams;Pwrenc];
% 
% U = jac(1:2*6*Nf+Nf+6+6+6,1+6              :6+2*6*Nf+Nf+6+6);
% P = jac(1:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6+6:6+2*6*Nf+Nf+6+6+6);
% 
% G = jac(1:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6+6+6 : 6+2*6*Nf+Nf+6+6+6+5*6+4*6+5);
% 
% % Gbeams = jac(1:2*6*Nf+Nf,1+6+2*6*Nf+Nf+6:6+2*6*Nf+Nf+6+6+6+5*6+4*6+5);
% % Gwrenc = jac(1+2*6*Nf+Nf+6:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6:6+2*6*Nf+Nf+6+6+6+5*6+4*6+5);
% % G = [Gbeams;Gwrenc];
% 
% Z = null(G');
% 
% H = [U,P];
% Hr = Z'*H*Z;
% 
% % [~,flag] = chol(Hr);
% [flag,~,~] = checkEigenValues(Hr);
% if flag>0
%     a = 1;
% end

flag = 0;
end