function [x,y,z] = Circles(p,R,r,N)
th = linspace(0,2*pi,N);
x = zeros(N,1);
y = zeros(N,1);
z = zeros(N,1);

for i = 1:N
   xi = r*cos(th(i));
   yi = r*sin(th(i));
   vect_oriented = p+R*[xi;yi;0];
   x(i) = vect_oriented(1);
   y(i) = vect_oriented(2);
   z(i) = vect_oriented(3);
end

end