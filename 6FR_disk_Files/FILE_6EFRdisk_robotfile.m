%% Inverse Geometrico-Static problem
% assumed strain mode approach forward backward, 6EFR robot + 1
% intermediate constraint supported by a passive beam
% 16 Dec 2021 Federico Zaccaria

r = 0.001;
E = 210*10^9;
G = 80*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0;0];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench
Lp = 0.2;

actlim = [0.2,1];
Nleg = 6;
stresslim = + Inf;
%% Inverse problem data

roll = 0*pi/180;
pitch = 0*pi/180;
yaw = 0*pi/180;
rotparams = 'XYZ';

pp = [0;0;0.8];
qpd = [pp;roll;pitch;yaw];
params.pstart = pp;
%% GEOMETRY
rb = 0.15;
rp = 0.08;
rd = 0.12;

db = 0.02;
dp = 0.02;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;
th4 =  30*pi/180;
th5 = 150*pi/180;
th6 = 270*pi/180;

angles = [th1,th2,th3];

b1 = Rz(th1)*[0;rb;0]; % WRT base frame
b2 = Rz(th2)*[0;rb;0];
b3 = Rz(th3)*[0;rb;0];

b1A1 = +Rz(th1)*[db;0;0];
b1A2 = -Rz(th1)*[db;0;0];
b2A3 = +Rz(th2)*[db;0;0];
b2A4 = -Rz(th2)*[db;0;0];
b3A5 = +Rz(th3)*[db;0;0];
b3A6 = -Rz(th3)*[db;0;0];

A1 = b1 +b1A1;
A2 = b1 +b1A2;
A3 = b2 +b2A3;
A4 = b2 +b2A4;
A5 = b3 +b3A5;
A6 = b3 +b3A6;
A7 = zeros(3,1);

basepoints = [A1,A2,A3,A4,A5,A6,A7];

p1 = Rz(th4)*[rp;0;0]; % WRT platform frame
p2 = Rz(th5)*[rp;0;0];
p3 = Rz(th6)*[rp;0;0];

p1B1 = +Rz(th4)*[0;dp;0];
p1B2 = -Rz(th4)*[0;dp;0];
p2B3 = +Rz(th5)*[0;dp;0];
p2B4 = -Rz(th5)*[0;dp;0];
p3B5 = +Rz(th6)*[0;dp;0];
p3B6 = -Rz(th6)*[0;dp;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B4,B3,B6,B5,B2];

baseangles = [angles(1),angles(1),angles(2),angles(2),angles(3),angles(3),0]; %ZYX convention
          
% disk geometry

C1 = Rz(060*pi/180)*[rd;0;0];
C2 = Rz(120*pi/180)*[rd;0;0];
C3 = Rz(180*pi/180)*[rd;0;0];
C4 = Rz(240*pi/180)*[rd;0;0];
C5 = Rz(300*pi/180)*[rd;0;0];
C6 = Rz(000*pi/180)*[rd;0;0];
C7 = zeros(3,1);
diskpoints = [C1,C2,C3,C4,C5,C6,C7];


geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.diskpoints = diskpoints;
geometry.rotparam = rotparams;

%% Solution
Nf = 4;
qa0 = 0.8*ones(6,1);
qe0 = 0.001*rand(2*6*3*Nf + 3*Nf,1);
L10 = 0.4*ones(6,1);
qd0 = [0;0;0.4;0;0;0];
qp0 = [pp;roll;pitch;yaw];
lambda0 = 0.001*rand(6*5+4*6+5,1);
guess0 = [qa0;qe0;L10;qd0;qp0;lambda0];

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false);
M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) InverseMode6EFR_1c(guess,geometry,Kad,qpd,wp,wd,Nf,Lp);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);
params.y0 = sol;
%% PLOT & SHAPE RECOVERY
posbeams = pos6EFR_1c(sol,geometry,Nf,Lp);

qd = sol(1+6+6*3*Nf*2+3*Nf+6:6+6*3*Nf*2+3*Nf+6+6,1);
pdisk = qd(1:3,1);
[Rdisk,~,~,~] = rotationParametrization(qd(4:6,1),rotparams);

qp = sol(1+6+6*3*Nf*2+3*Nf+6+6:6+6*3*Nf*2+3*Nf+6+6+6,1);
pplat = qp(1:3,1);
[Rplat,~,~,~] = rotationParametrization(qp(4:6,1),rotparams);

Plot6EFR_1c(geometry,posbeams,pplat,Rplat,pdisk,Rdisk,rd,rp,rb);

drawnow
%% Simulation functions
fcn.objetivefcn = @(y,pend)  InverseMode6EFR_1c_mex(y,geometry,Kad,[pend;roll;pitch;yaw],wp,wd,Nf,Lp);
fcn.mechconstrfcn = @(y) mechconstr6EFR_1c(y,actlim,r,E,Nf,Nleg,stresslim);
fcn.singufcn = @(jac,y) SingularityMode6EFR_1c(jac,Nf,y,rotparams);
fcn.stabilityfcn = @(jac,y) StabilityMode6EFR_1c(jac,Nf,y,rotparams);
