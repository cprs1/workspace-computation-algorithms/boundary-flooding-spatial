function  Plot6EFR_1c(geometry,posbeams,pplat,Rplat,pdisk,Rdisk,rd,rp,rb)

basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platformpoints = geometry.platpoints;
diskpoints = geometry.diskpoints;

posbeamsb = posbeams(1:3*6,:);
posbeamsd = posbeams(1+3*6:6*6,:);
posbeampassive = posbeams(1+6*6:6*6+3,:);

% plot fixed frames
plot3SdR(zeros(3,1),eye(3),1/5)

platpoint = zeros(3,7);

for i = 1:6
   platpoint(:,i) = pplat + Rplat*platformpoints(:,i);
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bd','LineWidth',1.5)
   plot3(platpoint(1,i),platpoint(2,i),platpoint(3,i),'rs','LineWidth',1.5) 
   yi1 = posbeamsb(1+3*(i-1):3*i,:);
   plot3(yi1(1,:),yi1(2,:),yi1(3,:),'k','LineWidth',1.5)
   yi2 = posbeamsd(1+3*(i-1):3*i,:);
   plot3(yi2(1,:),yi2(2,:),yi2(3,:),'k','LineWidth',1.5)
   diskpoint = pdisk + Rdisk*diskpoints(:,i);
   
   [xc,yc,zc] = Circles(diskpoint,Rdisk,rd/10,50);
   plot3(xc,yc,zc,'r','LineWidth',1.5)
    
end

% Passive leg
   i = 7;
   platpoint(:,i) = pplat + Rplat*diskpoints(:,i);
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bd','LineWidth',1.5)
   plot3(pdisk(1),pdisk(2),pdisk(3),'rs','LineWidth',1.5) 
   yi1 = posbeampassive;
   plot3(yi1(1,:),yi1(2,:),yi1(3,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',1.5)

% plot intermediate disk
[xd,yd,zd] = Circles(pdisk,Rdisk,rd,100);
plot3(xd,yd,zd,'k','LineWidth',1.5)

% plot platform frame
plot3SdR(pplat,Rplat,1/5)

% plot platform
[xd,yd,zd] = Circles(pplat,Rplat,rp,100);
plot3(xd,yd,zd,'r','LineWidth',1.5)

% plot baseform
[xd,yd,zd] = Circles(zeros(3,1),eye(3),rb,100);
plot3(xd,yd,zd,'b','LineWidth',1.5)

axis equal
axis([-1 1 -1 1 0 1])
grid minor

end