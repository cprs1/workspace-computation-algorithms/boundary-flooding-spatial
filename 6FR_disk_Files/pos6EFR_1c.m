function posbeams = pos6EFR_1c(guess,geometry,Nf,Lp)
Nftot = 3*Nf;
Nsh = 100;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
qa = guess(1:6,1);
qeb = guess(1+6:6+6*Nftot,1);
qed = guess(1+6+6*Nftot:6+2*6*Nftot,1);
qepassive = guess(1+6+2*6*Nftot: 6+ 2*6*Nftot + Nftot,1);
L1 = guess(1+6+2*6*Nftot + Nftot:6+2*6*Nftot + Nftot+6,1);

posbeamsb = zeros(6*3,Nsh);
posbeamsd = zeros(6*3,Nsh);


for i = 1:6
    %% FIRST STEP
    % extract variables
    Lb = L1(i);
    p01 = basepoints(:,i);
    h01 = eul2quat([baseangles(i),0,0],'ZYX')';
    qe1 = qeb(1+3*Nf*(i-1):i*3*Nf,1);
    % integrate
    y01 = [p01;h01];
    fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,Lb);
    [s1,y1] = ode45(fun1,[0,1],y01);
    % spline results
    sspan1 = linspace(0,Lb,Nsh);
    posbeamsb(1+3*(i-1):3*i,:) = spline(Lb*s1,y1(:,1:3)',sspan1);
    %% SECOND STEP
    % extract variables
    Ld = qa(i)-L1(i);
    p02 = y1(end,1:3)';
    h02 = y1(end,4:7)';
    qe2 = qed(1+3*Nf*(i-1):i*3*Nf,1);
    % integrate
    y02 = [p02;h02];
    fun2 = @(s,y) OdeFunReconstruct(s,y,qe2,Nf,Ld);
    [s2,y2] = ode45(fun2,[0,1],y02);
    % spline results
    sspan2 = linspace(0,Ld,Nsh);
    posbeamsd(1+3*(i-1):3*i,:) = spline(Ld*s2,y2(:,1:3)',sspan2);
end

i = 7;
p0p = basepoints(:,i);
h0p = eul2quat([baseangles(i),0,0],'ZYX')';
% integrate
y0p = [p0p;h0p];
funp = @(s,y) OdeFunReconstruct(s,y,qepassive,Nf,Lp);
[sp,yp] = ode45(funp,[0,1],y0p);

% spline results
sspanp = linspace(0,Lp,Nsh);
posbeampassive = spline(Lp*sp,yp(:,1:3)',sspanp);

%%
posbeams = [posbeamsb;posbeamsd;posbeampassive];

end