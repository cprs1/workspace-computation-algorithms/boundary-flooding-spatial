%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

%% INPUT FILE

%% Simulation parameters

maxiter = 20;
TOL = 10^-5;
TOL2 = Inf;
stepsize_x = 0.01;
stepsize_y = 0.01;
stepsize_z = 0.01;
boxsize = [-.85 +.85 -.85 +.85  0 1];

n_rad = 2;
tau = 10;


%% STORE VARIABLES

params.n_rad = n_rad;
params.stepsize_x = stepsize_x;
params.stepsize_y = stepsize_y;
params.stepsize_z = stepsize_z;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.tau = tau;

