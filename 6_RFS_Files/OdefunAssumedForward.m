function yd = OdefunAssumedForward(s,y,qei,Nf,L,str)
Nftot = 3*Nf;

h = y(4:7,1);
dhdqa = y(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
dhdqe = reshape( y(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
R = quat2rotmatrix(h);
Phi = PhiMatr(s,L,Nf);
k = Phi*qei;
k1 = k(1); k2 = k(2); k3 = k(3);
A = [0,-k1,-k2,-k3;
    +k1,0,+k3,-k2;
    +k2,-k3,0,+k1;
    +k3,+k2,-k1,0];
Dk = [-h(2),-h(3),-h(4);+h(1),-h(4),+h(3);+h(4),+h(1),-h(2);-h(3),+h(2),+h(1)];
Dh = 2*[+h(3),+h(4),+h(1),+h(2);-h(2),-h(1),+h(4),+h(3);+h(1),-h(2),-h(3),+h(4)];

switch str
    case 'fix'        
        pd = R(:,3);
        hd = 0.5*A*h;

        dpdqad = Dh*dhdqa;
        dpdqed = Dh*dhdqe;
        dhdqad = 0.5*A*dhdqa;
        dhdqed = 0.5*(Dk*Phi+A*dhdqe);
        
        yd = L*[pd;hd;dpdqad;reshape(dpdqed,3*Nftot,1);dhdqad;reshape(dhdqed,4*Nftot,1)];

    case 'variable'
        pd = L*R(:,3);
        hd = L*0.5*A*h;

        dpdqad = L*Dh*dhdqa + R(:,3);
        dpdqed = L*Dh*dhdqe;
        dhdqad = 0.5*(L*A*dhdqa + A*h);
        dhdqed = L*0.5*(Dk*Phi+A*dhdqe);
        
        yd = [pd;hd;dpdqad;reshape(dpdqed,3*Nftot,1);dhdqad;reshape(dhdqed,4*Nftot,1)];
    otherwise
        error('Define settings for beams length');
end

end