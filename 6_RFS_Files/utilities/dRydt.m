% DERIVATIVE OF ROTATION MATRIX Y WRT ITS ANGLE
function R = dRydt(t)
R = [-sin(t),0,cos(t);
     0      ,0,0     ;
     -cos(t),0,-sin(t)];
end