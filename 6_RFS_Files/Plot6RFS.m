function h = Plot6RFS(geometry,qa,posbeams,pplat,Rplat)
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platformpoints = geometry.platpoints;

% figure()
% plot fixed frames
yvect = [0;1;0]/5;
xvect = [1;0;0]/5;
zvect = [0;0;1]/5;
quiver3(0,0,0,xvect(1),xvect(2),xvect(3),'Color','r','LineWidth',1.5)
hold on
quiver3(0,0,0,yvect(1),yvect(2),yvect(3),'Color','b','LineWidth',1.5)
quiver3(0,0,0,zvect(1),zvect(2),zvect(3),'Color','k','LineWidth',1.5)

platpoint = zeros(3,6);
h = [];
for i = 1:6
   platpoint(:,i) = pplat + Rplat*platformpoints(:,i);
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bo','LineWidth',1.5)
   h1 = plot3(platpoint(1,i),platpoint(2,i),platpoint(3,i),'ro','LineWidth',1.5); 
   xlocal = Rz(baseangles(i))*Rx(qa(i,1))*xvect;
   ylocal = Rz(baseangles(i))*Rx(qa(i,1))*yvect;
   zlocal = Rz(baseangles(i))*Rx(qa(i,1))*zvect;
   h2 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),xlocal(1),xlocal(2),xlocal(3),'Color','r','LineWidth',1.5);
   h3 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),ylocal(1),ylocal(2),ylocal(3),'Color','b','LineWidth',1.5);
   h4 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),zlocal(1),zlocal(2),zlocal(3),'Color','k','LineWidth',1.5);
   yi = posbeams(1+3*(i-1):3*i,:);
   h5 = plot3(yi(1,:),yi(2,:),yi(3,:),'k','LineWidth',1.5);
   h = [h;h1;h2;h3;h4;h5];
end

% plot platform frame
xplat = Rplat*xvect;
yplat = Rplat*yvect;
zplat = Rplat*zvect;
h1 = quiver3(pplat(1),pplat(2),pplat(3),xplat(1),xplat(2),xplat(3),'Color','r','LineWidth',1.5);
h2 = quiver3(pplat(1),pplat(2),pplat(3),yplat(1),yplat(2),yplat(3),'Color','b','LineWidth',1.5);
h3 = quiver3(pplat(1),pplat(2),pplat(3),zplat(1),zplat(2),zplat(3),'Color','k','LineWidth',1.5);

% plot platform
h4 = line([platpoint(1,1),platpoint(1,2)],[platpoint(2,1),platpoint(2,2)],[platpoint(3,1),platpoint(3,2)],'LineWidth',1.5);
h5 = line([platpoint(1,2),platpoint(1,3)],[platpoint(2,2),platpoint(2,3)],[platpoint(3,2),platpoint(3,3)],'LineWidth',1.5);
h6 = line([platpoint(1,3),platpoint(1,4)],[platpoint(2,3),platpoint(2,4)],[platpoint(3,3),platpoint(3,4)],'LineWidth',1.5);
h7 = line([platpoint(1,4),platpoint(1,5)],[platpoint(2,4),platpoint(2,5)],[platpoint(3,4),platpoint(3,5)],'LineWidth',1.5);
h8 = line([platpoint(1,5),platpoint(1,6)],[platpoint(2,5),platpoint(2,6)],[platpoint(3,5),platpoint(3,6)],'LineWidth',1.5);
h9 = line([platpoint(1,6),platpoint(1,1)],[platpoint(2,6),platpoint(2,1)],[platpoint(3,6),platpoint(3,1)],'LineWidth',1.5);

h = [h;h1;h2;h3;h4;h5;h6;h7;h8;h9];

axis equal
axis([-1 1 -1 1 -1 1])
grid on

end