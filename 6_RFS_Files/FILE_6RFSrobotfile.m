%% Robot Geometry Parameters

rb = 0.4;
rp = 0.2;
dp = 0.05;

th1 =   0*pi/180;
th2 =  60*pi/180;
th3 = 120*pi/180;
th4 = 180*pi/180;
th5 = 240*pi/180;
th6 = 300*pi/180;

angles = [th1,th2,th3,th4,th5,th6]+pi/2;

A1 = Rz(th1)*[rb;0;0];
A2 = Rz(th2)*[rb;0;0];
A3 = Rz(th3)*[rb;0;0];
A4 = Rz(th4)*[rb;0;0];
A5 = Rz(th5)*[rb;0;0];
A6 = Rz(th6)*[rb;0;0];

basepoints = [A1,A2,A3,A4,A5,A6];

th1p =  30*pi/180;
th2p = 150*pi/180;
th3p = 270*pi/180;

p1 = Rz(th1p)*[rp;0;0]; % WRT platform frame
p2 = Rz(th2p)*[rp;0;0];
p3 = Rz(th3p)*[rp;0;0];

p1B1 = -Rz(th1p)*[0;dp;0];
p1B2 = +Rz(th1p)*[0;dp;0];
p2B3 = -Rz(th2p)*[0;dp;0];
p2B4 = +Rz(th2p)*[0;dp;0];
p3B5 = -Rz(th3p)*[0;dp;0];
p3B6 = +Rz(th3p)*[0;dp;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B2,B3,B4,B5,B6];

baseangles = angles';

% reorient points

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparams = 'XYZ';

%% Robot Material Parameters
r = 0.001; % beams cross section radius [m]
A = pi*r^2;
I = 0.25*pi*r^4; % inertial moment of area [m^4]
L = 1;
J = 2*I;
E = 210*10^9; % young modulus [Pa]
EI = E*I; 
G = 80*10^9; % shear modulus [Pa]
Kbt = diag([EI;EI;G*J]);
stresslim = +1800*10^6;

%% EXTERNAL LOADS
g = [0;0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0;-.1];
l = zeros(3,1);
mext = [0;0;0];

wd = [l;f]; % distributed wrench
wp = [mext;fext]; % platform wrench


%% Model Parameters
Nf = 4;
Nleg = 6;

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);


roll = 0*pi/180;
pitch = 0*pi/180;
yaw = 0*pi/180;

hp = [roll,pitch,yaw]';

actlim = [-Inf*ones(Nleg,1),+Inf*ones(Nleg,1)];

pstart = [0;0;0.62];
params.pstart = pstart;


%% Simulation functions
fcn.objetivefcn = @(y,pend)  InverseMode6RFS_mex(y,geometry,L,Kad,[pend;hp],wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr6RFS(y,actlim,r,E,Nf,Nleg,stresslim);
fcn.singufcn = @(jac,y) SingularityMode6RFS(jac,Nf,y,geometry.rotparams);
fcn.stabilityfcn = @(jac,y) StabilityMode6RFS(jac,Nf,y,geometry.rotparams);


%% First Initial Guess

qa0 = 15*pi/180 * ones(6,1);
A0 = [-1;zeros(3*Nf-1,1)];
qe0 = [+A0;+A0;+A0;+A0;+A0;+A0];
qp0 = [pstart;hp];
lambda0 = 0.001*rand(3*Nleg,1);
y0 = [qa0;qe0;qp0;lambda0];

fun = @(y) InverseMode6RFS(y,geometry,L,Kad,[pstart;hp],wp,wd,Nf);
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false);
[sol,~,~,~,jac] = fsolve(fun,y0,options);
% sol = y0;
%%
posbeams = pos6RFS(sol,geometry,Nf,L);

qa = sol(1:6,1);
pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),geometry.rotparams);

h = Plot6RFS(geometry,qa,posbeams,pplat,Rplat);
title('First Iteration Configuration')

drawnow
params.y0 = sol;
