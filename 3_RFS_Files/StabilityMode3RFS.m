function flag = StabilityMode3RFS(jac,Nfm,y,rotparams)
Nf = 3*Nfm;
eul = y(1+3+3*Nf+3:3+3*Nf+3+3,1);
D = rot2twist(eul,rotparams);
jac(1+3*Nf:3*Nf+3,:) = D'*jac(1+3*Nf:3*Nf+3,:);

Uorient1 =  jac(1:3*Nf+6,1+3+3*Nf+3:3+3*Nf+6);
Uelastic1 = jac(1:3*Nf+6,1+3:3+3*Nf);
U = [Uelastic1,Uorient1];
% 
P = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+3);

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+3*Nf+6+3*3);
Z = null(G');

H = [U,P];
% H = jac(1:3*Nf+6,1+3:3+3*Nf+6);
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end