function yd = OdeFunReconstruct(s,y,qei,Nf,L)

h = y(4:7,1);
R = quat2rotmatrix(h);

k = PhiMatr(s,L,Nf)*qei;
k1 = k(1); 
k2 = k(2);
k3 = k(3);
A = [0,-k1,-k2,-k3;
     +k1,0,+k3,-k2;
     +k2,-k3,0,+k1;
     +k3,+k2,-k1,0];
 
pd = R(:,3);
hd = 0.5*A*h;

yd = L*[pd;hd];
end