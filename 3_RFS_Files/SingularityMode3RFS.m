function [flag1,flag2] = SingularityMode3RFS(jac,Nfm,y,rotparams)

Nf = 3*Nfm;
eul = y(1+3+3*Nf+3:3+3*Nf+3+3,1);
D = rot2twist(eul,rotparams);
jac(1+3*Nf:3*Nf+3,:) = D'*jac(1+3*Nf:3*Nf+3,:);

A1 = jac(1:3*Nf+6,1:3);
Uorient1 = jac(1:3*Nf+6,1+3+3*Nf+3:3+3*Nf+6);
Uelastic1 = jac(1:3*Nf+6,1+3:3+3*Nf);
U1 = [Uelastic1,Uorient1];
P1 = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+3);

A2 = jac(1+3*Nf+6:3*Nf+6+3*3,1:3);
Uelastic2 = jac(1+3*Nf+6:3*Nf+6+3*3,1+3:3+3*Nf);
Uorient2 = jac(1+3*Nf+6:3*Nf+6+3*3,1+3+3*Nf+3:3+3*Nf+6);
U2 =[Uelastic2,Uorient2];
P2 = jac(1+3*Nf+6:3*Nf+6+3*3,1+3+3*Nf:3+3*Nf+3);

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+3*Nf+6+3*3);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*U1,Z'*P1 ;
         U2,   P2];


flag1 = rcond(T1);
flag2 = rcond(T2);

end