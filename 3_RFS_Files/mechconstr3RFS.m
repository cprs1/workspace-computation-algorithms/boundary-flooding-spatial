function flag = mechconstr3RFS(y,actlim,r,E,Nf,stresslim)
qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qA3 = wrapTo2Pi(y(3,1));



flag1 = (actlim(1)<=qA1 & qA1<=actlim(2));      % motor constraint 1
flag2 = (actlim(1)<=qA2 & qA2<=actlim(2));  	% motor constraint 2
flag3 = (actlim(1)<=qA3 & qA3<=actlim(2));      % motor constraint 3
flagm = (flag1 & flag2 & flag3);

flag = 1;
k = 1;
while k<=3 && flag==1
    qe = y(1+3+3*Nf*(k-1):3+3*Nf*k,1);
    Nsamp = 100; % sample in 100 points
    s = linspace(0,1,Nsamp);
    i = 1;
    while i<=Nsamp && flag==1
        M = PhiMatr(s(i),1,Nf);
        strain = M*qe;
        sigma1 = r*E*strain(1);
        sigma2 = r*E*strain(2); % neglect torsion
        s1 = max(((sigma1).^2.).^0.5);
        s2 = max(((sigma2).^2.).^0.5);
        stress = max([s1,s2]);
        flag_stress = stress<=stresslim;
        flag = (flagm && flag_stress);
        i = i+1;
    end
    k = k+1;
end
end