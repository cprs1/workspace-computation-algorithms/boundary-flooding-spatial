%% Robot Geometry Parameters
close all

rb = 0.4;
rp = 0.2;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;

baseangles = [th1,th2,th3];

A1 = Rz(th1)*[0;rb;0]; % WRT base frame
A2 = Rz(th2)*[0;rb;0];
A3 = Rz(th3)*[0;rb;0];

basepoints = [A1,A2,A3];

B1 = Rz(th1)*[0;rp;0]; % WRT platform frame
B2 = Rz(th2)*[0;rp;0];
B3 = Rz(th3)*[0;rp;0];

platformpoints = [B1,B2,B3];

rotparams = 'XYZ'; % not use the others, it may pass from 0,0,0

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparams = rotparams;
%% Robot Material Parameters
r = 0.001;
E = 210*10^9;
G = 80*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
L = 1;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
stresslim = +1800*10^6;

%% EXTERNAL LOADS
f = rho*A*g;
fext = [0;0;-.1];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Model Parameters
Nf = 4;

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

pstart = [0;0;0.8];
params.pstart = pstart;

actlim = [-Inf; + Inf];
%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseMode3RFS_mex(y,geometry,L,Kad,pend,wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr3RFS(y,actlim,r,E,Nf,stresslim);
fcn.singufcn = @(jac,y) SingularityMode3RFS(jac,Nf,y,rotparams);
fcn.stabilityfcn = @(jac,y) StabilityMode3RFS(jac,Nf,y,rotparams);


%% First Initial Guess

qa0 = zeros(3,1);
qe0 = zeros(3*3*Nf,1);
qp0 = pstart;
qporient = zeros(3,1);
lambda0 = zeros(3*3,1);
y0 = [qa0;qe0;qp0;qporient;lambda0];

fun = @(y) InverseMode3RFS(y,geometry,L,Kad,pstart,wp,wd,Nf);
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true);
[sol,~,~,~,jac] = fsolve(fun,y0,options);

%%
posbeams = pos3RFS(sol,geometry,Nf,L);

qa = sol(1:3,1);
pplat = sol(1+3+3*3*Nf:3+3*3*Nf+3,1);
qporient = sol(1+3+3*Nf*3+3:3+3*Nf*3+3+3,1);

[Rplat,~,~,~] = rotationParametrization(qporient,rotparams);
h = Plot3RFS(geometry,qa,posbeams,pplat,Rplat);
title('First Iteration Configuration');

drawnow
params.y0 = sol;
