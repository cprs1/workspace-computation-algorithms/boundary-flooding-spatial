function [eq,gradeq] = InverseMode6EFS(guess,geometry,Kee,qpd,wp,wd,Nf)
Nftot = 3*Nf;

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
baseangles = geometry.baseangles;
rotparams = geometry.rotparams;

qa = guess(1:6,1);
qe = guess(1+6:6+6*Nftot,1);
qp = guess(1+6+6*Nftot:6+6*Nftot+6,1);
lambda = guess(1+6+6*Nftot+6:6+6*Nftot+6+3*6,1);

pplat = qp(1:3,1);
[Rp,dRpdroll,dRpdpitch,dRpdyaw] = rotationParametrization(qp(4:6,1),rotparams);


Ci = [zeros(3,3);eye(3,3)];

% matrix initialization
beameq = zeros(6*Nftot,1);
dbeamdqa = zeros(6*Nftot,6);
dbeamdqe = zeros(6*Nftot,6*Nftot);
dbeamdlambd = zeros(6*Nftot,3*6);
geomconstr = zeros(3*6,1);
dconsdqa = zeros(3*6,6);
dconsdqe = zeros(3*6,6*Nftot);
dconsdqp = zeros(3*6,6);

wrencheq = wp; % platform frame equilibrium
dwrenchdqp = zeros(6,6);
dwrenchdlambd = zeros(6,3*6);

for k = 1:6
    % BEAM  INTEGRATION
    % extract variables
    L = qa(k,1);
    p0 = basepoints(:,k);
    p1 = platpoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];

    h0 = eul2quat([baseangles(k),0,0],'ZYX')';
    qei = qe(1+Nftot*(k-1):k*Nftot,1);
    lambdai = lambda(1+3*(k-1):3*k,1);
    wrench = (Ci*lambdai);
    
    % integrate forward
    y0F = [p0;h0;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'variable');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    pL = ygeom(1:3,1);
    hL = ygeom(4:7,1);
    dpLdqa = ygeom(1+7:7+3,1);
    dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
    dhLdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    
    % integrate backward
    Rtip = quat2rotmatrix(hL);
    Adg = [Rtip',zeros(3); zeros(3), Rtip']; % ONLY Rotate in local frame
    lwrench = Adg*(-wrench);
    [~,~,~,D1t,D2t,D3t] = derivativeColRotMatQuat(hL);
    matr2 = [wrench(1:3,1)'*D1t;wrench(1:3,1)'*D2t;wrench(1:3,1)'*D3t;wrench(4:6,1)'*D1t;wrench(4:6,1)'*D2t;wrench(4:6,1)'*D3t;];
    dw0dqa = -matr2*dhLdqa;
    dw0dqe = -matr2*dhLdqe;
    y0B = [lwrench;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'variable');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+6:6+Nftot,1);
    dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    dQcdqa = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1) + dQcdw0*dw0dqa;
    dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot) + dQcdw0*dw0dqe;
    
    % platform equibribrium contributions    
    pwrench =  -wrench; % wrench in plat frame

    Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; % translate to platform origin

    wrencheq = wrencheq + Adg2*pwrench; % equilibrium in platform frame     
  
    mat1 = -[skew(dppdrot(:,1))*wrench(4:6,1),skew(dppdrot(:,2))*wrench(4:6,1),skew(dppdrot(:,3))*wrench(4:6,1)];
    dwrenchdqp = dwrenchdqp +Adg2*[zeros(3,3),mat1;zeros(3,6)];
   
    
    % equations and gradient
    beameq(1+Nftot*(k-1):Nftot*k,1) = L*Kee*qei + Qc;
    dbeamdqa(1+Nftot*(k-1):Nftot*k,k) = Kee*qei + dQcdqa;
    dbeamdqe(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L*Kee + dQcdqe;
    dbeamdlambd(1+Nftot*(k-1):Nftot*k,1+3*(k-1):3*k) = - dQcdw0*Adg*Ci;
    geomconstr(1+3*(k-1):3*k,1)  = pL-pLp;
    dconsdqa(1+3*(k-1):3*k,k) = dpLdqa;
    dconsdqe(1+3*(k-1):3*k,1+Nftot*(k-1):Nftot*k) = dpLdqe;
    dconsdqp(1+3*(k-1):3*k,:) = -[eye(3),dppdrot];
    dwrenchdlambd(:,1+3*(k-1):3*k) = - Adg2*Ci;

end


% inverse problem

inv = qp-qpd;

% collect

eq = [beameq;wrencheq;geomconstr;inv];

gradeq = [dbeamdqa,    dbeamdqe,         zeros(6*Nftot,6),  dbeamdlambd;
          zeros(6,6),  zeros(6,6*Nftot), dwrenchdqp,        dwrenchdlambd;
          dconsdqa,    dconsdqe,         dconsdqp,          zeros(3*6,6*3);
          zeros(6,6),  zeros(6,6*Nftot), eye(6,6),          zeros(6,6*3);];
end