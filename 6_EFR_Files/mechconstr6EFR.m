function flag = mechconstr6EFR(y,actlim,r,E,Nf,stresslim)

k = 1;
flag = 1;
while k<=6 && flag==1
    qa = y(k,1);
    qe = y(1+6+3*Nf*(k-1):6+3*Nf*k,1);
    % actuator limits
    flag_act = (actlim(1)<=qa & qa<=actlim(2));     
    % strain limits
    Nsamp = 100; % sample in 100 points
    s = linspace(0,1,Nsamp);
    i = 1;
    while i<=Nsamp && flag==1
        M = PhiMatr(s(i),1,Nf);
        strain = M*qe;
        sigma1 = r*E*strain(1);
        sigma2 = r*E*strain(2); % neglect torsion
        s1 = max(((sigma1).^2.).^0.5);
        s2 = max(((sigma2).^2.).^0.5);
        stress = max([s1,s2]);
        flag_stress = stress<=stresslim;
        flag = (flag_act && flag_stress);
        i = i+1;
    end
    k = k+1;
end