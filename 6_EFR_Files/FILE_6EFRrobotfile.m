%% Robot Geometry Parameters
close all

rb = 0.3;
rp = 0.2;
db = 0.1;
dp = 0.05;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;
th4 =  30*pi/180;
th5 = 150*pi/180;
th6 = 270*pi/180;

angles = [th1,th2,th3];

b1 = Rz(th1)*[0;rb;0]; % WRT base frame
b2 = Rz(th2)*[0;rb;0];
b3 = Rz(th3)*[0;rb;0];

b1A1 = +Rz(th1)*[db;0;0];
b1A2 = -Rz(th1)*[db;0;0];
b2A3 = +Rz(th2)*[db;0;0];
b2A4 = -Rz(th2)*[db;0;0];
b3A5 = +Rz(th3)*[db;0;0];
b3A6 = -Rz(th3)*[db;0;0];

A1 = b1 +b1A1;
A2 = b1 +b1A2;
A3 = b2 +b2A3;
A4 = b2 +b2A4;
A5 = b3 +b3A5;
A6 = b3 +b3A6;

basepoints = [A1,A2,A3,A4,A5,A6];

p1 = Rz(th4)*[rp;0;0]; % WRT platform frame
p2 = Rz(th5)*[rp;0;0];
p3 = Rz(th6)*[rp;0;0];

p1B1 = +Rz(th4)*[0;dp;0];
p1B2 = -Rz(th4)*[0;dp;0];
p2B3 = +Rz(th5)*[0;dp;0];
p2B4 = -Rz(th5)*[0;dp;0];
p3B5 = +Rz(th6)*[0;dp;0];
p3B6 = -Rz(th6)*[0;dp;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B4,B3,B6,B5,B2];

baseangles = [angles(1);angles(1);angles(2);angles(2);angles(3);angles(3)];

rotparams = 'XYZ';
geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparam = rotparams;

%% Robot Material Parameters
r = 0.001;
E = 210*10^9;
G = 80*10^9;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
stresslim = +1800*10^6;

%% EXTERNAL LOADS
f = rho*A*g;
fext = [0;0;-.1];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Model Parameters
Nf = 4;

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

roll = 0*pi/180;
pitch = 0*pi/180;
yaw = 0*pi/180;

hp = [roll,pitch,yaw]';

actlim = [0.2, 1];

pstart = [0;0;0.8];
params.pstart = pstart;


%% Simulation functions
fcn.objetivefcn = @(y,pend)  InverseMode6EFR_mex(y,geometry,Kad,[pend;hp],wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr6EFR(y,actlim,r,E,Nf,stresslim);
fcn.singufcn = @(jac,y) SingularityMode6EFR(jac,y,Nf,rotparams);
fcn.stabilityfcn = @(jac,y) StabilityMode6EFR(jac,y,Nf,rotparams);


%% First Initial Guess

qa0 = 0.7*ones(6,1);
qe0 = zeros(6*3*Nf,1);
qp0 = [pstart;roll;pitch;yaw];
lambda0 = zeros(5*6,1);
y0 = [qa0;qe0;qp0;lambda0];

fun = @(y) InverseMode6EFR(y,geometry,Kad,[pstart;hp],wp,wd,Nf);
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true);
[sol,~,~,~,jac] = fsolve(fun,y0,options);

%%
posbeams = pos6EFR(sol,geometry,Nf);

pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),rotparams);

h = Plot6EFR(geometry,posbeams,pplat,Rplat);title('First Iteration Configuration')

drawnow
params.y0 = sol;
