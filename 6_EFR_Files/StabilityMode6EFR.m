function flag = StabilityMode6EFR(jac,y,Nfm,rotparams)
Nf = 3*Nfm;

eul = y(1+6+6*3*Nfm+3:6+6*3*Nfm+6,1);
D = rot2twist(eul,rotparams);
jac(1+6*Nf+3:6*Nf+6,:) = D'*jac(1+6*Nf+3:6*Nf+6,:);

U = jac(1:6*Nf+6,1+6:6+6*Nf);
P = jac(1:6*Nf+6,1+6+6*Nf:6+6*Nf+6);

G = jac(1:6*Nf+6,1+6+6*Nf+6:6+6+6*Nf+6*5);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end